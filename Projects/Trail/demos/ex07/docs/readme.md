# Spring Data - CRUD Operations

## What are CRUD Operations?

Basically we do four things in programming. We

- create objects,
- read objects,
- update objects and
- delete objects.

That is **C**reate - **R**ead - **U**pdate - **D**elete.


## The CRUD Repository

*Spring Data* provides a special interface for the CRUD operations,
the ``CrudRepository<T, I>`` interface. If our repository interface extends
that interface, then *Spring Data* generates the functionality for
the CRUD operations.


## Identifying the ID

Since *Spring Data* cannot know, which property or which properties contain
the ID value(s), we need to help *Spring Data*. By annotation the ID
properties, *Spring Data* knows, which ones to take.
