package demo.spring.data.jdbc;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

/**
 * <h1>The Application Configuration</h1>
 * 
 * @author Franz Tost
 */
@Configuration
@EnableJdbcRepositories
public class ApplicationConfiguration { }
