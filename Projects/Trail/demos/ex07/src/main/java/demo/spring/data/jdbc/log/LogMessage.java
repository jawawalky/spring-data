package demo.spring.data.jdbc.log;

import org.springframework.data.annotation.Id;

/**
 * <h1>The Log Message Entity</h1>
 * 
 * @author Franz Tost
 *
 */
public class LogMessage {
	
	// fields /////
	
	@Id private Long   id;
	private     String message;
	
	
	// constructors /////
	
	public LogMessage() { }
	
	public LogMessage(final String message) {
		
		this();
		
		this.message = message;
		
	}

	
	// methods /////
	
	public Long   getId()              { return this.id; }
	public void   setId(final Long id) { this.id = id;   }

	public String getMessage()                  { return this.message; }
	public void   setMessage(final String name) { this.message = name; }


	@Override public String toString() {
		
		return String.format("[%5d] %s", this.getId(), this.getMessage());
		
	}

}
