package demo.spring.data.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.customer.Customer;
import demo.spring.data.jdbc.customer.CustomerService;
import demo.spring.data.jdbc.log.LogService;

/**
 * <h1>The Application</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LogService logService;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override
	public void run(final String... args) throws Exception {
		
		// Creating Customers /////
		
		System.out.println("A) Correct Transaction ...");
		
		try {
			
			final Customer bob = new Customer("Bob Builder",  "New York");
			final Customer sue = new Customer("Sue Smart",    "Vancouver");
			final Customer ann = new Customer("Ann Analyzer", "London");
			this.customerService.saveGroup(bob, sue, ann);
			
		} // try
		catch (RuntimeException e) {
			
			this.logService.log("Transaction failed");
			
		} // catch
		
		this.printLogMessages();
		this.printCustomers();
		
		
		System.out.println("B) Failed Transaction ...");
		
		try {
			
			final Customer joe = new Customer("Joe Jumper",    "Copenhagen");
			final Customer lou = new Customer("Lou Large",     "Kuala Lumpur");
			final Customer kim = new Customer("Kim Knowledge", "Paris");
			this.customerService.saveGroup(joe, lou, kim);
			
		} // try
		catch (RuntimeException e) {
			
			this.logService.log("Transaction failed");
			
		} // catch
		
		this.printLogMessages();
		this.printCustomers();
		
		System.out.println("Finished.");
		
	}
	
	private void printCustomers() {
		
		System.out.println("Customers:");
		
		this.customerService
			.customers()
			.forEach(c -> System.out.println("  " + c));
		
	}

	private void printLogMessages() {
		
		System.out.println("Log Messages:");
		
		this.logService
			.messages()
			.forEach(m -> System.out.println("  " + m));
		
	}

}
