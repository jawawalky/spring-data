package demo.spring.data.jdbc.customer;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * <h1>The Customer Repository</h1>
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository
	extends PagingAndSortingRepository<Customer, Long>
{ }
