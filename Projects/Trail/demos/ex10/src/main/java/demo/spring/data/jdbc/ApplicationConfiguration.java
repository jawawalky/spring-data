package demo.spring.data.jdbc;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jdbc.repository.config.EnableJdbcAuditing;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

/**
 * <h1>The Application Configuration</h1>
 * 
 * @author Franz Tost
 */
@Configuration
@EnableJdbcRepositories
@EnableJdbcAuditing
public class ApplicationConfiguration {
	
	// methods /////
	
	@Bean
	AuditorAware<String> auditor() {
		
		return () -> Optional.of("franz");
		
	}
	
}
