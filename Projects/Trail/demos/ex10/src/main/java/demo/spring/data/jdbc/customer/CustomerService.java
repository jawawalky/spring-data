package demo.spring.data.jdbc.customer;

import static java.util.stream.Collectors.joining;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import demo.spring.data.jdbc.log.LogService;

/**
 * <h1>The Customer Service</h1>
 * 
 * @author Franz Tost
 *
 */
@Transactional @Service           // <- Denotes that all methods of
public class CustomerService {    //    this service should be run within
                                  //    transactions.
                                  //    The default transaction propagation
                                  //    mode is REQUIRED.
	
	// fields /////
	
	@Autowired
	private LogService logService;
	
	@Autowired
	private CustomerRepository repository;
	

	// methods /////
	
	public void saveGroup(final Customer... customers) {
		
		final String names =
			Arrays.stream(customers)
				.map(c -> c.getName())
				.collect(joining(", "));
		
		this.logService.log("Saving " + names + " ...");
		
		for (Customer customer : customers) {
			
			this.repository.save(customer);
			
		} // for
		
	}
	
	public Iterable<Customer> customers() {
		
		return this.repository.findAll();
		
	}
	
}
