CREATE TABLE Customer (
    id   INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    city VARCHAR(10),
    created_By VARCHAR(20),
    created_Date TIMESTAMP,
    last_Modified_By VARCHAR(20),
    last_Modified_Date TIMESTAMP
);

CREATE TABLE Log_Message (
    id      INTEGER PRIMARY KEY AUTO_INCREMENT,
    message VARCHAR(100)
);
