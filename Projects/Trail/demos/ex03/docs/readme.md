# Spring Data - Queries

## How to Define a Query?

Simple queries or queries of medium complexity can easily be defined by adding
query methods to the repository interface. The query methods must follow
a certain naming convention, because the actual query is derived from
the method name.


## Examples

- `Address getByStreet(String streetName)`
- `Optional<Address> findByStreet(String streetName)`
- `List<Address> queryByStreetLikeAndHouseNoGreaterThan(String streetNamePattern, int minHouseNo)`
- `List<Address> queryByCityOrderByStreetAsc(String cityName)`
