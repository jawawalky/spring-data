package demo.spring.data.jdbc;

import static java.time.Month.APRIL;
import static java.time.Month.AUGUST;
import static java.time.Month.JANUARY;
import static java.time.Month.JULY;
import static java.time.Month.MARCH;
import static java.time.Month.NOVEMBER;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.address.Address;
import demo.spring.data.jdbc.customer.Customer;
import demo.spring.data.jdbc.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		this.populateDatabase();
		
		
		// Find Customer by Name /////
		
		System.out.println("A) Finding customer by name ...");
		
		Optional<Customer> customer = this.customerRepository.getByName("Sue Smart");
		if (customer.isPresent()) {
			
			System.out.println("Found: " + customer.get());
			
		} // if
		
		customer = this.customerRepository.getByName("Duke");
		if (customer.isEmpty()) {
			
			System.out.println("Not found: Duke");
			
		} // if

		
		// Find All Customers born after 1995 /////
		
		System.out.println("B) Finding all customers born after 1995 ...");
		
		this.customerRepository
			.queryByDateOfBirthGreaterThanEqual(LocalDate.of(1996, JANUARY, 1))
			.forEach(c -> System.out.println(c.getName() + " " + c.getDateOfBirth()));		

		
		// Order Customers in Ascending Sequence /////
		
		System.out.println("C) Ordering customers ascending ...");
		
		this.customerRepository
			.queryByNameLikeOrderByDateOfBirthAsc("J%")
			.forEach(c -> System.out.println(c.getName() + " " + c.getDateOfBirth()));		

		
		// Order Customers in Descending Sequence /////
		
		System.out.println("D) Ordering customers descending ...");
		
		this.customerRepository
			.queryByNameLikeOrderByDateOfBirthDesc("J%")
			.forEach(c -> System.out.println(c.getName() + " " + c.getDateOfBirth()));		

		
		// Apply Logic Operators /////
		
		System.out.println("E) Applying Logic Operators ...");
		
		this.customerRepository
			.findByNameLikeAndDateOfBirthBetween("J%", LocalDate.of(1970, JANUARY, 1), LocalDate.of(1980, JANUARY, 1))
			.forEach(c -> System.out.println(c.getName() + " " + c.getDateOfBirth()));		

	}
	
	private void populateDatabase() {
		
		this.customerRepository.save(
			new Customer(
				"Bob Builder",
				LocalDate.of(1980, NOVEMBER, 7),
				new Address("5th Avenue", "28438", "New York")
			)
		);
		
		this.customerRepository.save(
			new Customer(
				"Sue Smart",
				LocalDate.of(1999, MARCH, 12),
				new Address("Snow Drive 87", "77399", "Vancouver")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Ann Analyzer",
				LocalDate.of(1991, JULY, 8),
				new Address("Oxford Road 34", "11MW BC23", "London")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Lin Li",
				LocalDate.of(2001, NOVEMBER, 19),
				new Address("Nang Gin Raod 227", "110 218", "Beijing")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Carlos Calculador",
				LocalDate.of(1993, AUGUST, 30),
				new Address("Calle del Sol", "21002", "Madrid")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Jimmy Speed",
				LocalDate.of(1997, JULY, 10),
				new Address("Baker Street 221b", "12SW GT88", "London")
			)
		);
				
		this.customerRepository.save(
			new Customer(
				"Joe Busy",
				LocalDate.of(1972, APRIL, 1),
				new Address("Silk Road", "73329", "Boston")
			)
		);
					
	}

}
