package demo.spring.data.jdbc.customer;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries of simple and medium complexity can be defined by giving query
 * methods appropriate names.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> getByName(String name);
	
	List<Customer> queryByDateOfBirthGreaterThanEqual(LocalDate date);
	
	List<Customer> queryByNameLikeOrderByDateOfBirthAsc(String namePattern);
	
	List<Customer> queryByNameLikeOrderByDateOfBirthDesc(String namePattern);
	
	List<Customer> findByNameLikeAndDateOfBirthBetween(String namePattern, LocalDate minDate, LocalDate maxDate);
	
}
