package demo.spring.data.jdbc.address;

import org.springframework.beans.factory.annotation.Value;

/**
 * <h1>A Tag</h1>
 * 
 * An interface for an open projection. The label is composed of other
 * projection properties of the interface.
 * 
 * @author Franz Tost
 *
 */
public interface AddressTag {
	
	// methods /////
	
	@Value("#{target.street}, #{target.zipCode} #{target.city}")
	String getLabel();
	
}
