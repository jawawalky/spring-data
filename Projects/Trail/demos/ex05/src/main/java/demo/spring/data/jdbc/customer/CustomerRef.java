package demo.spring.data.jdbc.customer;

/**
 * <h1>A Customer Reference</h1>
 * 
 * A customer reference contains only the ID and the name of a customer.
 * All other information about a customer is neglected. The reference
 * is defined as an interface, which allows <i>Spring Data</i> to define
 * a projection on a {@link Customer} with the desired properties. 
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRef {
	
	// methods /////
	
	Long getId();            // <- Direct projections of properties from
	                         //    the actual domain type.
	String getName();

}
