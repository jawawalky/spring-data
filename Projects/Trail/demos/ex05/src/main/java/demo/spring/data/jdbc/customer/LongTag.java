package demo.spring.data.jdbc.customer;

import org.springframework.beans.factory.annotation.Value;

/**
 * <h1>A Tag</h1>
 * 
 * An interface for an open projection. The projected data is specified
 * using the <i>Value</i> annotation with Spring's expression language.
 * 
 * @author Franz Tost
 *
 */
public interface LongTag {
	
	// methods /////
	
	@Value(                                      // <- Defines the projected
		"#{target.getName() + ' ' + " +          //    data using Spring's
		"target.address.street + ', ' + " +      //    expression language.
		"target.address.zipCode + ' ' + " +
		"target.address.city}"
	)
	String getLabel();
	
}
