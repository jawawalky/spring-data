package demo.spring.data.jdbc.customer;

import demo.spring.data.jdbc.address.AddressTag;

/**
 * <h1>A Customer Tag</h1>
 * 
 * An interface for a projection with customer name and the address tag.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerTag {
	
	// methods /////
	
	String getName();
	
	AddressTag getAddress();
	
}
