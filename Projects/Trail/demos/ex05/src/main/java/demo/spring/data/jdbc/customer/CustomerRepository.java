package demo.spring.data.jdbc.customer;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries with different types of projections.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	CustomerRef getByName(String name);          // <- A simple, closed projection
	                                             //    of a Customer to his/her
	                                             //    name and street.
	
	<T> List<T> findBy(Class<T> projection);     // <- A dynamic projection, which
	                                             //    finds all customers
	                                             //    currently in the repository,
	                                             //    but returns only
	                                             //    the the properties
	                                             //    specified by the projection.
	
	<T> T queryById(
		Long     id,
		Class<T> projection
	);
	
}
