package demo.spring.data.jdbc.customer;

/**
 * <h1>A Label Provider</h1>
 * 
 * A projection can also be achieved by a factory method provided by some
 * bean registered in the application context. 
 * 
 * @author Franz Tost
 *
 */
public class TagLabelProvider {
	
	// methods /////
	
	public String getLabel(       // <- The factory method, which creates
		final Customer customer   //    a label for a tag with the data of
	) {                           //    a Customer.
		
		return
			"To be delivered to " +
			customer.getName()    +
			" living in "         +
			customer.getAddress().toString();
		
	}

}
