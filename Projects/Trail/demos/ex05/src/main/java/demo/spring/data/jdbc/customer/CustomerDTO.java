package demo.spring.data.jdbc.customer;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;

import demo.spring.data.jdbc.address.Address;

/**
 * <h1>A Customer Entity</h1>
 * 
 * @author Franz Tost
 *
 */
public class CustomerDTO {
	
	// fields /////
	
	private Long      id;
	private String    name;
	private LocalDate dateOfBirth;
	
	
	// methods /////
	
	public Long      getId()                                     { return this.id;                 }
	public void      setId(final Long id)                        { this.id = id;                   }

	public String    getName()                                   { return this.name;               }
	public void      setName(final String name)                  { this.name = name;               }

	public LocalDate getDateOfBirth()                            { return this.dateOfBirth;        }
	public void      setDateOfBirth(final LocalDate dateOfBirth) { this.dateOfBirth = dateOfBirth; }


	@Override public String toString() {
		
		return
			String.format(
				"[%d] %s (%s)",
				this.getId(),
				this.getName(),
				this.getDateOfBirth().toString()
			);
		
	}

}
