package demo.spring.data.jdbc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

import demo.spring.data.jdbc.customer.TagLabelProvider;

/**
 * <h1>The Configuration Class</h1>
 * 
 * The configuration is responsible for providing required bean instances and
 * bean proxies.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableJdbcRepositories()
public class ApplicationConfiguration {
	
	// methods /////
	
	@Bean                                        // <- We need to bind
	public TagLabelProvider tagLabelProvider() { //    the label provider of
		                                         //    the tag in the application
		return new TagLabelProvider();           //    context.
		
	}
	
}
