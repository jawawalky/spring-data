package demo.spring.data.jdbc.customer;

import org.springframework.beans.factory.annotation.Value;

/**
 * <h1>A Tag</h1>
 * 
 * An interface for an open projection. The label is composed of other
 * projection properties of the interface.
 * 
 * @author Franz Tost
 *
 */
public interface ShortTag {
	
	// methods /////
	
	String getName();
	
	@Value("#{target.address.city}")
	String getCity();
	
	default String getLabel() {                            // <- Uses other properties
		return this.getName() + " from " + this.getCity(); //    to compose the final
	}                                                      //    label.
	
}
