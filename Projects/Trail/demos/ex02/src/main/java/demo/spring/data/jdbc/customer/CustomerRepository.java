package demo.spring.data.jdbc.customer;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * The repository interface defines the actions on the persistence store.
 * This repository defines only a simple query, where a customer is identified
 * by his/her name.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository
	extends CrudRepository<Customer, Long>  // <- Offers the CRUD operations.
{

	// methods /////
	
	Customer getByName(final String name);

}
