package demo.spring.data.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.customer.Customer;
import demo.spring.data.jdbc.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired
	private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		// Creating Customers /////
		
		System.out.println("A) Creating customers ...");
		
		Customer bob    = this.customerRepository.save(new Customer("Bob Builder",       "New York"));
		Customer sue    = this.customerRepository.save(new Customer("Sue Smart",         "Vancouver"));
		Customer ann    = this.customerRepository.save(new Customer("Ann Analyzer",      "London"));
		Customer lin    = this.customerRepository.save(new Customer("Lin Li",            "Beijing"));
		Customer carlos = this.customerRepository.save(new Customer("Carlos Calculador", "Madrid"));
		
		System.out.println(bob);
		System.out.println(sue);
		System.out.println(ann);
		System.out.println(lin);
		System.out.println(carlos);
		
		
		// Find Customer by ID /////
		
		System.out.println("B) Finding customer by ID ...");
		
		Customer customer = this.customerRepository.findById(3L).get();
		System.out.println(customer);

		
		// Find Customer by Name /////
		
		System.out.println("C) Finding customer by name ...");
		
		customer = this.customerRepository.getByName("Sue Smart");
		System.out.println(customer);

		
		// Find All Customers /////
		
		System.out.println("D) Finding all customers ...");
		
		this.customerRepository.findAll().forEach(c -> System.out.println(c));		

		
		// Update a Customer /////
		
		System.out.println("E) Updating a customer ...");
		
		customer = this.customerRepository.findById(5L).get();
		customer.setCity("Barcelona");
		this.customerRepository.save(customer);
		customer = this.customerRepository.findById(5L).get();
		System.out.println(customer);
		
		
		// Delete Customer by ID /////
		
		System.out.println("F) Deleting customer by ID ...");
		
		this.customerRepository.deleteById(2L);
		this.customerRepository.deleteById(4L);
		this.customerRepository.findAll().forEach(c -> System.out.println(c));		
		
	}

}
