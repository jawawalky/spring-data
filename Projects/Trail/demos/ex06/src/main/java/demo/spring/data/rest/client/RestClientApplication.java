package demo.spring.data.rest.client;

import java.time.LocalDate;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.spring.data.jdbc.address.Address;
import demo.spring.data.jdbc.customer.Customer;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
public class RestClientApplication {
	
	// methods /////

	public static void main(final String[] args) {
		
		new RestClientApplication().runDemo();
		
	}
	
	private void runDemo() {
		
		System.out.println("Running REST client ...");
		
		
		System.out.println("A) Creating customer ...");
		
		this.restTemplate()
			.postForEntity(
				this.url(),
				new Customer(
					"Duke",
					LocalDate.parse("1968-06-18"),
					new Address("Java Boulevard 1", "12345", "Santa Monica")
				),
				Customer.class
			);
		this.printCustomer(15);
		

		System.out.println("B) Updating customer ...");
		
		Customer customer = this.getCustomer(15);
		customer.getAddress().setCity("Palo Alto");
		this.restTemplate().put(this.url() + "/15", customer);
		this.printCustomer(15);
		
		
		System.out.println("C) Removing customer ...");
		
		this.restTemplate().delete(this.url() + "/15");
		this.printCustomer(15);

		
		System.out.println("Finished.");
			
	}
	
	private RestTemplate restTemplate() {
		
		return new RestTemplate();
		
	}
	
	private String url() {
		
		return "http://localhost:8080/customers";
		
	}
	
	private Customer getCustomer(final int id) {
		
		return
			this.restTemplate()
				.getForEntity(this.url() + "/" + id, Customer.class)
				.getBody();
			
	}
	
	private void printCustomer(final int id) {
		
		try {
			
			System.out.println(this.getCustomer(id).toString());
			
		} // try
		catch (RestClientException e) {
			
			System.out.println(e.getMessage());
			
		} // catch

	}
	
}
