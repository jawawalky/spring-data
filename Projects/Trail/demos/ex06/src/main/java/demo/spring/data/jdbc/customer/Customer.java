package demo.spring.data.jdbc.customer;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;

import demo.spring.data.jdbc.address.Address;

/**
 * <h1>A Customer Entity</h1>
 * 
 * @author Franz Tost
 *
 */
public class Customer {
	
	// fields /////
	
	@Id private Long      id;
	private     String    name;
	private     LocalDate dateOfBirth;
	private     Address   address;
	
	
	// constructors /////
	
	public Customer() { }
	
	public Customer(
		final String    name,
		final LocalDate dateOfBirth,
		final Address   address
	) {
		
		this();
		
		this.name        = name;
		this.dateOfBirth = dateOfBirth;
		this.address     = address;
		
	}

	
	// methods /////
	
	public Long      getId()                                     { return this.id;                 }
	public void      setId(final Long id)                        { this.id = id;                   }

	public String    getName()                                   { return this.name;               }
	public void      setName(final String name)                  { this.name = name;               }

	public LocalDate getDateOfBirth()                            { return this.dateOfBirth;        }
	public void      setDateOfBirth(final LocalDate dateOfBirth) { this.dateOfBirth = dateOfBirth; }

	public Address   getAddress()                                { return this.address;            }
	public void      setAddress(final Address address)           { this.address = address;         }


	@Override public String toString() {
		
		return
			String.format(
				"%s (%s) from %s",
				this.getName(),
				this.getDateOfBirth(),
				this.getAddress().toString()
			);
		
	}

}
