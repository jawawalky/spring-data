package demo.spring.data.jdbc.address;

import org.springframework.data.annotation.Id;

/**
 * <h1>An Address Entity</h1>
 * 
 * @author Franz Tost
 *
 */
public class Address {
	
	// fields /////
	
	@Id private Long   id;
	private     String street;
	private     String zipCode;
	private     String city;
	
	
	// constructors /////
	
	public Address() { }
	
	public Address(
		final String street,
		final String zipCode,
		final String city
	) {
		
		this();
		
		this.street  = street;
		this.zipCode = zipCode;
		this.city    = city;
		
	}

	
	// methods /////
	
	public Long   getId()                          { return this.id;         }
	public void   setId(final Long id)             { this.id = id;           }

	public String getStreet()                      { return this.street;     }
	public void   setStreet(final String street)   { this.street = street;   }

	public String getZipCode()                     { return this.zipCode;    }
	public void   setZipCode(final String zipCode) { this.zipCode = zipCode; }

	public String getCity()                        { return this.city;       }
	public void   setCity(final String city)       { this.city = city;       }


	@Override public String toString() {
		
		return String.format(
			"%s, %s %s",
			this.getStreet(),
			this.getZipCode(),
			this.getCity()
		);
		
	}

}
