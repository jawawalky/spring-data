package demo.spring.data.jdbc;

import org.springframework.core.Ordered;
import org.springframework.data.relational.core.conversion.MutableAggregateChange;
import org.springframework.data.relational.core.mapping.event.BeforeSaveCallback;
import org.springframework.stereotype.Component;

import demo.spring.data.jdbc.customer.Customer;

/**
 * <h1>An Entity Callback</h1>
 * 
 * @author Franz Tost
 */
@Component
public class SecondCustomerBeforeSaveCallback
	implements
		BeforeSaveCallback<Customer>,
		Ordered
{
	
	// methods /////
	
	@Override
	public Customer onBeforeSave(
		final Customer                         customer,
		final MutableAggregateChange<Customer> aggregateChange
	) {
		
		System.out.println("Second : " + customer);
		return customer;
		
	}

	@Override
	public int getOrder() {
		
		return 2;
		
	}

}
