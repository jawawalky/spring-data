package demo.spring.data.jdbc.customer;

import org.springframework.data.annotation.Id;

/**
 * <h1>The Customer Entity</h1>
 * 
 * @author Franz Tost
 */
public class Customer {
	
	// fields /////
	
	@Id private Long   id;        // <- The annotation identifies the ID field.
	private     String name;
	private     String city;
	
	
	// constructors /////
	
	public Customer() { }
	
	public Customer(
		final String name,
		final String city
	) {
		
		this();
		
		this.name = name;
		this.city = city;
		
	}

	
	// methods /////
	
	public Long   getId()                    { return this.id;   }
	public void   setId(final Long id)       { this.id = id;     }

	public String getName()                  { return this.name; }
	public void   setName(final String name) { this.name = name; }

	public String getCity()                  { return this.city; }
	public void   setCity(final String city) { this.city = city; }


	@Override public String toString() {
		
		return String.format(
			"[%5d] %s from %s",
			this.getId(),
			this.getName(),
			this.getCity()
		);
		
	}

}
