package demo.spring.data.jdbc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.data.relational.core.mapping.event.BeforeSaveCallback;

import demo.spring.data.jdbc.customer.Customer;

/**
 * <h1>The Application Configuration</h1>
 * 
 * @author Franz Tost
 */
@Configuration
@EnableJdbcRepositories
public class ApplicationConfiguration {
	
	// methods /////
	
	@Bean
	public BeforeSaveCallback<Customer> customerBeforeSaveCallback() {

		return (c, ch) -> {
			System.out.println("Third " + c + " ...");
			return c;
		};
		
	}
	
}
