package demo.spring.data.jdbc;

import org.springframework.core.annotation.Order;
import org.springframework.data.relational.core.conversion.MutableAggregateChange;
import org.springframework.data.relational.core.mapping.event.BeforeSaveCallback;
import org.springframework.stereotype.Component;

import demo.spring.data.jdbc.customer.Customer;

/**
 * <h1>An Entity Callback</h1>
 * 
 * @author Franz Tost
 */
@Component @Order(1)
public class FirstCustomerBeforeSaveCallback
	implements
		BeforeSaveCallback<Customer>
{
	
	// methods /////
	
	@Override
	public Customer onBeforeSave(
		final Customer                         customer,
		final MutableAggregateChange<Customer> aggregateChange
	) {
		
		System.out.println("First : " + customer);
		return customer;
		
	}

}
