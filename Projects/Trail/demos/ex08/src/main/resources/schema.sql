CREATE TABLE Customer (
    id   INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    city VARCHAR(10)
);

CREATE TABLE Log_Message (
    id      INTEGER PRIMARY KEY AUTO_INCREMENT,
    message VARCHAR(100)
);
