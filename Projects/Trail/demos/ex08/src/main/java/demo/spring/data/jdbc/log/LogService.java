package demo.spring.data.jdbc.log;

import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <h1>The Log Service</h1>
 * 
 * @author Franz Tost
 *
 */
@Service
public class LogService {
	
	// fields /////
	
	@Autowired
	private LogRepository repository;
	

	// methods /////

	@Transactional(propagation = REQUIRES_NEW)   // <- Says that this method
	public void log(final String message) {      //    should run within its
                                                 //    own transaction.
		
		this.repository.save(new LogMessage(message));
		
	}
	
	public Iterable<LogMessage> messages() {
		
		return this.repository.findAll();
		
	}
	
}
