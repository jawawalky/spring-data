package demo.spring.data.jdbc.log;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * <h1>The Log Repository</h1>
 * 
 * @author Franz Tost
 *
 */
public interface LogRepository
	extends PagingAndSortingRepository<LogMessage, Long>
{ }
