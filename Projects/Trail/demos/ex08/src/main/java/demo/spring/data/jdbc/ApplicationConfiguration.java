package demo.spring.data.jdbc;

import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.data.relational.core.mapping.event.AfterSaveEvent;
import org.springframework.data.relational.core.mapping.event.BeforeSaveEvent;

/**
 * <h1>The Application Configuration</h1>
 * 
 * @author Franz Tost
 */
@Configuration
@EnableJdbcRepositories
public class ApplicationConfiguration {
	
	// methods /////
	
	@Bean
	public ApplicationListener<BeforeSaveEvent<Object>> beforeSaveEvent() {

		return e -> System.out.println("Before saving " + e.getEntity() + " ...");
		
	}
	
	@Bean
	public ApplicationListener<AfterSaveEvent<Object>> afterSaveEvent() {

		return e -> System.out.println("After saving " + e.getEntity() + ".");
		
	}
	
}
