package demo.spring.data.jdbc.customer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries with dynamic.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> getByName(String name);
	
	List<Customer> queryByNameLike(String namePattern, Sort sort);
	
	List<Customer> findAll(Pageable pageable);
	
}
