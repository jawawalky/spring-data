# Spring Data - Sorting

## Declaring Sortability

Sorting data can easily be achieved by adding a parameter of type `Sort`
to a query function of the repository interface.

So if we define

```
List<Person> queryByHeightLessThan(int height, Sort sort);
```

instead of

```
List<Person> queryByHeightLessThan(int height);
```

then we can have the elements of the result list sorted.


## Ordering Results

When sorting has been declared on the query method, then we are ready to use
it in our code. Let's say, we wanted to sort the results by name,
then the following code would do

```
List<Person> persons = repository.queryByHeightLessThan(1.40, Sort.by("name").ascending());
```


## Predefined Query Methods

There are some query methods known to *Spring Data*, such as `findAll()`.
They can also be equipped with sorting, by adding the `Sort` parameter.

So querying all persons with sorting would be

```
List<Person> findAll(Sort sort);
```

instead of

```
List<Person> findAll();
```


# Spring Data - Paging

## Declaring Pagability

Paging data can easily be achieved by adding a parameter of type `Pageable`
to a query function of the repository interface.

So if we define

```
List<Person> queryByHeightLessThan(int height, Pageable pageable);
```

instead of

```
List<Person> queryByHeightLessThan(int height);
```

then we can retrieve the results by pages.


## Paging through Results

When paging has been declared on the query method, then we are ready to use
it in our code.

```
List<Person> page1 = repository.queryByHeightLessThan(1.40, PageRequest.of(0, 5));
List<Person> page2 = repository.queryByHeightLessThan(1.40, PageRequest.of(2, 5));
...
```

Each page contains five results.


## Predefined Query Methods

There are some query methods known to *Spring Data*, such as `findAll()`.
They can also be equipped with paging, by adding the `Pageable` parameter.

So querying all persons with paging would be

```
List<Person> findAll(Pageable pageable);
```

instead of

```
List<Person> findAll();
```
