package demo.spring.data.jdbc.customer;

import org.springframework.data.repository.Repository;

/**
 * <h1>The Repository Class</h1>
 * 
 * The repository interface defines the actions on the persistence store.
 * This repository defines only a simple query, where a customer is identified
 * by his/her name.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository
	extends Repository<Customer, Long>      // <- Denotes this class as a repository, which
{                                           //    will be supported by a proxy implementation
	                                        //    by Spring Data.
	// methods /////
	
	Customer getByName(final String name);  // <- Defines a query, which will retrieve
	                                        //    a customer by his/her name. If a JDBC
	                                        //    proxy were created, then an appropriate
	                                        //    SQL SELECT statement would be generated.
	                                        //    If we were using some other persistence
	                                        //    layer, e.g. JPA or a No-SQL-database,
	                                        //    then a query in their genuine query syntax
	                                        //    would be generated.

}
