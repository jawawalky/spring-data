package demo.spring.data.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.customer.Customer;
import demo.spring.data.jdbc.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication                      // <- Triggers scanning for components, configurations etc.
public class DemoApplication
	implements CommandLineRunner            // <- Provides the 'run(...)' method, which is invoked after
{                                           //    the application has booted.
	
	// fields /////
	
	@Autowired
	private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override
	public void run(final String... args)   // <- Here we can place our application code that
		throws Exception                    //    should run after the application has booted.
	{
		
		Customer customer =                 // <- Performs a query on the database, which
			this.customerRepository         //    retrieves the record for 'Sue Smart'.
				.getByName("Sue Smart");    //    The record data is materialized as
		                                    //    an object of type 'Customer'.
		
		System.out.println(customer);
		
	}

}
