INSERT INTO Customer VALUES (1, 'Bob Builder',       'New York');
INSERT INTO Customer VALUES (2, 'Sue Smart',         'Vancouver');
INSERT INTO Customer VALUES (3, 'Ann Analyzer',      'London');
INSERT INTO Customer VALUES (4, 'Lin Li',            'Beijing');
INSERT INTO Customer VALUES (5, 'Carlos Calculador', 'Madrid');
