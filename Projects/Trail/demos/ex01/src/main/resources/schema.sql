CREATE TABLE Customer (
    id   INTEGER PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    city VARCHAR(50)
);
