# Spring Data - Getting Started

## Object Mapping

*Spring Data* must be able to create instances of our domain objects.
The domain objects are those objects, which are stored in some kind of
persistent storage, such as a relational database, a key-value-store,
a NoSQL-database etc.

## Constructor Selection

Since all these storages use different technologies to store domain objects,
*Spring Data* uses different ways to read them from those storages and to write
them to them. When it reads the data of a domain object from some storage,
it needs to know, how to materialize an object from the read data, i.e. it
must know which constructor to use.

An appropriate constructor is chosen in the following way

1. If the domain class has only one constructor, then that constructor is used.
1. If the domain class has multiple constructors and one constructor is
annotated with `@PersistenceConstructor`, then that constructor will be used.
1. Finally, if none of the previous rules hold, then *Spring Data* will use
the default constructor (= no arguments).

The resolution of the constructor arguments is based on the names of
the properties read from the data storage, e.g. the column names etc.
That only works, if parameter name information is available in the class file.
Otherwise the parameter names have to be provided by the annotation
`@ConstructorProperties`.

**Examples**

The first example demonstrates a domain object class, which has only one
constructor

```
public class Customer {

    @ConstructorProperties({"id", "name", "city"})
    public Constructor(Long id, String name, String city) { ... }
    
    // ...

}
```

Since the actual parameter names may be erased in the byte code, we provide
them, using the `@ConstructorProperties` annotation.

If there is more than one constructor, we need to tell *Spring Data*, which
constructor to use by annotating the desired constructor with
`@PersistenceConstructor`

```
public class Customer {

    public Constructor(String name, String city) { ... }
 
    @PersistenceConstructor   
    @ConstructorProperties({"id", "name", "city"})
    public Constructor(Long id, String name, String city) { ... }
    
    // ...

}
```

Often we work with *JavaBeans*, also called *POJOs*. Then there is a default
constructor and *getter*- and *setter*-methods for the properties. In that case
no annotations are needed, since the default constructor is for the creation of
the domain object and the *setter*-methods are used for the population of
the properties of the domain object.

```
public class Customer {

    private Long   id;
    private String name;
    
    // ...
    
    
    public Long getId() { ... }
    public void setId(final Long id) { ... }
    
    // ...

}
```

### Store-specific Value Resolution

Depending on the used storage type, it my be necessary to use the `@Value`
annotation of the *Spring Framework* to specify store-specific *SpEL*
expressions for the resolution of stored values.

### Object Creation Internals

Instead of using reflection, *Spring Data* creates for each domain class
a factory at runtime, which calls the constructor targeted for object creation.

So for the class

```
public class Customer {

    @ConstructorProperties({"id", "name", "city"})
    public Constructor(Long id, String name, String city) { ... }
    
    // ...

}
```

a factory class

```
class CustomerObjectInstantiator implements ObjectInstantiator {

    Object newInstance(Object... args) {
        return new Customer((Long) args[0], (String) args[1], (String) args[2]);
    }
    
}
```

implementing the interface `ObjectInstantiator` will be generated.

In order for this to work, the domain class must meet the following
requirements

- it must not be `private`.
- it must not be an inner, non-static class.
- it must not be a *CGLib* proxy class.
- the constructor used by *Spring Data* must not be `private`.

In case one or more of those requirements are not met, then *Spring Data*
uses reflection.

## Property Population

Once an instance has been created by *Spring Data* it will populate
the remaining properties. Appropriate ways to populate properties are given
by the following rules

- Immutable properties are set using an appropriate `with...(...)` method, if
it exists. It creates a new instance with the current properties and the new
property.
- If accessible *getter*- and *setter*-methods are available,
then the *setter*-methods are used for setting the property values.
- Mutable properties are set directly through the fields containing
the property values.
- Immutable properties can also be set, if an appropriate constructor with
that property value exists.

## Repositories

The main concept of *Spring Data* is the `Repository`. For each entity type
we define a separate repository. It contains all database actions for a certain
entity type, such as queries, create-, read-, update- or delete-actions.

We can define a repository for some entity type by deriving an interface
from the interface `Repository`, which is generic. If we take our entity class

```
public class Customer {

    private Long   id;
    private String name;
    
    // getter- and setter-methods
    
}
```

then we can define a repository for that class by

```
public CustomerRepository extends Repository<Customer, Long> { }
```

> The first generic parameter represents the entity type and the second
the type of its primary key.

### Queries


