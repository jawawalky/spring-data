# Spring Data - Queries

## Defining Queries

[CustomerRepository](./src/main/java/demo/spring/data/jdbc/customer/CustomerRepository.java)

Define the following queries

1. Query all customers, who were born after a certain date.
   Hint: Use `LocalDate` for the date value.
1. Query all customers, whose name matches a certain pattern and order
   them by date-of-birth in ascending order.
1. Query all customers, whose name matches a certain pattern and order
   them by date-of-birth in descending order.
1. Query all customers, whose name matches a certain pattern and
   who were born in a certain time interval delimited by two dates.

 
## Using Queries

[CustomerRepository](./src/main/java/demo/spring/data/jdbc/DemoApplication.java)

Perform the following queries

1. Look for *Sue Smart* and print her details on the console.
1. Look for *Duke* and report, if he could not be found.
1. Find all customers, who were born after 1995 and print them on the console.
1. Find all customers, whose name starts with *J*. Print them on the console
   ordered ascending by their birth date. (Hint: The pattern is `J%`).
1. Find all customers, whose name starts with *J*. Print them on the console
   ordered descending by their birth date. (Hint: The pattern is `J%`).
1. Find all customers, whose name starts with *J* and who were born between
   1970 and 1980.
