package demo.spring.data.jdbc.customer;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries of simple and medium complexity can be defined by giving query
 * methods appropriate names.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> getByName(String name);
	
	// TODO
	//
	//  Define the following queries
	//
	//  o Query all customers, who were born after a certain date.
	//
	//    Hint: Use 'LocalDate' for the date value.
	//
	//  o Query all customers, whose name matches a certain pattern and order
	//    them by date-of-birth in ascending order.
	//
	//  o Query all customers, whose name matches a certain pattern and order
	//    them by date-of-birth in descending order.
	//
	//  o Query all customers, whose name matches a certain pattern and
	//    who were born in a certain time interval delimited by two dates.
	//
	
}
