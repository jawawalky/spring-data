package demo.spring.data.jdbc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 * 
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////

	// TODO
	//
	//  o Inject an instance of the 'CustomerRepository'.
	//
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		// TODO
		//
		//  o Use the customer repository to retrieve the customer
		//    'Sue Smart'.
		//
		//  o Print the details of 'Sue Smart' on the console.
		//
		
	}

}
