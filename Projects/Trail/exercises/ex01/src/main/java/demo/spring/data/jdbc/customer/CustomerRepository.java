package demo.spring.data.jdbc.customer;

/**
 * <h1>The Repository Class</h1>
 * 
 * The repository interface defines the actions on the persistence store.
 * This repository defines only a simple query, where a customer is identified
 * by his/her name.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the interface 'CustomerRepository' extend the interface
	  'org.springframework.data.repository.Repository' with the correct
	  data types for the generic type parameters.
 *
 */
public interface CustomerRepository {
	
	// methods /////
	
	// TODO
	//
	//  o Add an appropriate method, which can retrieve a customer from
	//    the database by his/her name.
	//

}
