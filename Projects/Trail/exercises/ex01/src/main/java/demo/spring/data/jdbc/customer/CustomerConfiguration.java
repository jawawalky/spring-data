package demo.spring.data.jdbc.customer;

/**
 * <h1>The Configuration Class</h1>
 * 
 * The configuration is responsible for providing required bean instances and
 * bean proxies.
 * 
 * @author Franz Tost
 *
 * TODO
 * 
 *  o Make the class a *Spring* configuration.
 *  
 *  o Trigger the scan for repository classes and the creation of JDBC proxy
 *    implementations for them.
 */
public class CustomerConfiguration { }
