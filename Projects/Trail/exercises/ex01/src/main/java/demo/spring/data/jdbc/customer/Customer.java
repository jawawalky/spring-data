package demo.spring.data.jdbc.customer;

/**
 * <h1>The Entity Class</h1>
 * 
 * An entity defines a persistable object. Usually the properties of
 * the class are the persisted values. Compare the name of the class with
 * the name of the table and the types and the names of the properties with
 * the columns of the table, defined in <i>schema.sql</i>.
 * 
 * @author Franz Tost
 *
 * TODO
 * 
 *  o Add the following properties
 *  
 *     o id: Long
 *     o name: String
 *     o city: String
 *     
 *  o Add an appropriate 'toString()' method.
 */
public class Customer {
	
	// fields /////
	
	
	// methods /////
	
}
