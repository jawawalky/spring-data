# Spring Data - Getting Started

## What it is All About?

We have a very simple *Spring Boot* application, which retrieves a record
from a database table and materializes an entity object from the record
data.

The following technologies are being deployed

- *Spring Boot*
- *Spring Data*
- *H2 In-Memory Database*


## The Project

### The Application

The application is a *Spring Boot* application. The main application class
is ``DemoApplication``. That's where the application starts. *Spring Boot*
initializes the application, creates *Spring Beans*, *Configurations* etc.
and applies dependency injection.


### The Database

Since the *H2* database library is on the class path, *Spring Boot*
automatically provides an in-memory database, which we can use without
any further configuration. The database can be exchanged later, if we
want to replace it by some really persisting database system.

Further *Spring Boot* provides two files ``schema.sql`` and ``data.sql``,
which can be used to setup tables and to fill these tables with data
at the start-up of the application.


### The Persistence Layer

Objects that we want to persist in our database are called *entities*.
In this case the entity class is ``Customer``. It is a *POJO* or
*JavaBean*. So there is not very much more to be done, than defining
properties, which are the values to be persisted.

Access to the persistence layer/repository is provided by so called
*repository interfaces*. Here ``CustomerRepository`` is the repository
interface for our entity ``Customer``. It defines the actions to be
performed on the persistence layer.

The actual implementation of repository interfaces is done by
*Spring Data*. This happens in a configuration. Our configuration class
is ``CustomerConfiguration``.


## Running the Application

The application is a regular *Java* program. So just start it in your IDE
or on a terminal window, as you are used to.
