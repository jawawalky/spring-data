# Spring Data - Getting Started

## The Entity Class

`Customer`:

1. Create an entity class with the following properties
    - id: Long
    - name: String
    - city: String
    
1. Add an appropriate `toString()` method.


## The Repository Class

`CustomerRepository`:

1. Let the interface `CustomerRepository` extend the interface
   `org.springframework.data.repository.Repository` with the correct
   data types for the generic type parameters.
   
2. Add an appropriate method, which can retrieve a customer from the database
   by his/her name.
   
   
## The Configuration Class

`CustomerConfiguration`:

1. Make the class a *Spring* configuration.

1. Trigger the scan for repository classes and the creation of JDBC proxy
   implementations for them.
   
**Hint:** It can all be done only with annotations.


## The Application Class

`DemoApplication`:

1. Inject an instance of the 'CustomerRepository'.

1. Use the customer repository to retrieve the customer 'Sue Smart'.

1. Print the details of 'Sue Smart' on the console.

1. Run the program.
