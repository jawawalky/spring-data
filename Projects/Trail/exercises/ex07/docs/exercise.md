# Transactions

Usually we try to use declarative transactions in our applications. Declarative
transactions are purely done by configuration, either by annotations or by XML.
If we are using *Spring Boot* and an appropriate *Spring Data* module, then
a transaction manager is already in place and we can move straight forward and
configure the transaction behavior of our application code.

> If we use *Spring* without *Spring Boot*, then we need to configure the database
and the transaction manager in our application configuration.

Furthermore the standard *CRUD* repositories are transactional. So they will
work perfectly within transactions.

## What We Want to Do

In this exercise we want to write some `Customer` records to the database.
It should be possible to write a group of records within one transaction.
The effect will be that either all records can be written or, if at least
one record fails, no record of the group will be added to the database.

## Writing a Service

We will start off by writing a service, which allows us to save a group
of `Customer`s.

Create a new class `demo.spring.data.jdbc.customer.CustomerService`.

Let the class be a *service* class by adding the appropriate *Spring*
annotation. That will bind the service in the application context.

Add a method `saveGroup(Customer... customers)`, which saves all group
members in the database.

> Use the `CustomerRepository` to save each member of the group.

Write another method `customers()`, which returns a collection with all
`Customer`s available in the database.

Make the whole service **transactional**.

## Persisting Groups of Customers in the Database

Open [DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java).

Inject the `CustomerService` that just created.

Go to section *A)* and save the first group of customers, consisting of

- Bob Builder, New York
- Sue Smart, Vancouver
- Ann Analyzer, London

Verify that all customers have been written to the database

> Hint: Read them from the database and print them on the console.

Now go to section *B)* and save the second group of customers, consisting of

- Joe Jumper, Copenhagen
- Lou Large, Kuala Lumpur
- Kim Knowledge, Paris

Saving *Lou Large* from *Kuala Lumpur* will fail, since *Kuala Lumpur* is
too large for the column *city*.

> Hint: The failed transaction will throw an exception, so wrap your code
in a `try-catch` block.

Verify that no customer of the second group has been written to the database.
The customers of the first group should still be in the database.

## AddOn: Log Service

Optionally create a second set of interfaces and classes for the following
purpose. It should be possible to write log messages to the database.
The table for log messages has already been created
(see [schema.sql](../src/main/resources/schema.sql)).

Add a package `demo.spring.data.jdbc.log`.

Add to that package the following classes and interfaces

- `LogMessage` (entity class)
- `LogRepository` (repository interface)
- `LogService` (service class)

> Hint: Implement the classes and interfaces similar to those of `Customer`.

The `LogService` should have one method, which allows us to write a log
message and another message, which lists all saved messages.

The `log(...)` method should be transactional, but should run in its own
transaction, independent of the transaction running in the calling context.

> Hint: Set the appropriate transaction propagation flag.

Write log messages, when a group of `Customer`s should be saved and when
that action fails.
