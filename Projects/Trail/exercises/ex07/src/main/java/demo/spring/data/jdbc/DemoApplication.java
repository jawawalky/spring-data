package demo.spring.data.jdbc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h1>The Application</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	// TODO
	//
	//  o Inject the 'CustomerService'.
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override
	public void run(final String... args) throws Exception {
		
		// Creating Customers /////
		
		System.out.println("A) Correct Transaction ...");
		
		// TODO
		//
		//  o Write the following group of customers to the database
		//
		//      o Bob Builder, New York
		//      o Sue Smart, Vancouver
		//      o Ann Analyzer, London
		//
		//  o Print the saved customers on the console.
		
		
		System.out.println("B) Failed Transaction ...");
		
		// TODO
		//
		//  o Write the following group of customers to the database
		//
		//      o Joe Jumper, Copenhagen
		//      o Lou Large, Kuala Lumpur
		//      o Kim Knowledge, Paris
		//
		//  o Print the saved customers on the console.
		
		
		System.out.println("Finished.");
		
	}
	
}
