package demo.spring.data.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		// Creating Customers /////
		
		System.out.println("A) Creating customers ...");
		
		// TODO
		//
		//  o Create the following customers
		//
		//     o 'Bob Builder' from 'New York'
		//     o 'Sue Smart' from 'Vancouver'
		//     o 'Ann Analyzer' from 'London'
		//     o 'Lin Li' from 'Beijing'
		//     o 'Carlos Calculador' from 'Madrid'
		//
		//    and persist them in the database.
		//
		//  o Print their values on the console. Check, if they got correct
		//    IDs.
		//
		
		
		// Find Customer by ID /////
		
		System.out.println("B) Finding customer by ID ...");
		
		// TODO
		//
		//  o Find the customer with the ID '3'.
		//
		//  o Print the customer on the console.
		//
		
		
		// Find Customer by Name /////
		
		System.out.println("C) Finding customer by name ...");
		
		// TODO
		//
		//  o Find the customer with the name 'Sue Smart'.
		//
		//  o Print the customer on the console.
		//
		
		
		// Find All Customers /////
		
		System.out.println("D) Finding all customers ...");
		
		// TODO
		//
		//  o Find all customers.
		//
		//  o Print the customers on the console.
		//
		
		
		// Update a Customer /////
		
		System.out.println("E) Updating a customer ...");
		
		// TODO
		//
		//  o Find the customer with ID '5'.
		//
		//  o Change the city of the customer to 'Barcelona'.
		//
		//  o Find the customer with ID '5' again.
		//
		//  o Print the customer on the console.
		//
		
		
		// Delete Customer by ID /////
		
		System.out.println("F) Deleting customer by ID ...");
		
		// TODO
		//
		//  o Delete the customers with ID '2' and '4'.
		//
		//  o Print all remaining customers on the console.
		//
		
	}

}
