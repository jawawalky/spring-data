package demo.spring.data.jdbc.customer;

import org.springframework.data.annotation.Id;

/**
 * <h1>The Entity Class</h1>
 * 
 * An entity defines a persistable object. Usually the properties of
 * the class are the persisted values. Compare the name of the class with
 * the name of the table and the types and the names of the properties with
 * the columns of the table, defined in <i>schema.sql</i>.
 * 
 * @author Franz Tost
 *
 */
public class Customer {
	
	// fields /////
	
	@Id private Long   id;        // <- The annotation identifies the ID field.
	private     String name;
	private     String city;
	
	
	// constructors /////
	
	public Customer() { }
	
	public Customer(
		final String name,
		final String city
	) {
		
		this();
		
		this.name = name;
		this.city = city;
		
	}

	
	// methods /////
	
	public Long   getId()                    { return this.id;   }
	public void   setId(final Long id)       { this.id = id;     }

	public String getName()                  { return this.name; }
	public void   setName(final String name) { this.name = name; }

	public String getCity()                  { return this.city; }
	public void   setCity(final String city) { this.city = city; }


	@Override public String toString() {
		
		return String.format(
			"[%5d] %s from %s",
			this.getId(),
			this.getName(),
			this.getCity()
		);
		
	}

}
