package demo.spring.data.jdbc.customer;

import org.springframework.data.repository.Repository;

/**
 * <h1>The Repository Class</h1>
 * 
 * The repository interface defines the actions on the persistence store.
 * This repository defines only a simple query, where a customer is identified
 * by his/her name.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Replace the 'Repository' by a 'CrudRepository'.
 */
public interface CustomerRepository
	extends Repository<Customer, Long>
{

	// methods /////
	
	Customer getByName(final String name);

}
