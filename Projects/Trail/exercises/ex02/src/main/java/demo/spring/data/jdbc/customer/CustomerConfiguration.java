package demo.spring.data.jdbc.customer;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

/**
 * <h1>The Configuration Class</h1>
 * 
 * The configuration is responsible for providing required bean instances and
 * bean proxies.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableJdbcRepositories
public class CustomerConfiguration { }
