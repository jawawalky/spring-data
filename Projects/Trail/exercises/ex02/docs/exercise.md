# Spring Data - CRUD Repositories

## The Repository Interface

``CustomerRepository``:

1. Replace the ``Repository<T, I>`` by a ``CrudRepository<T, I>`` in
   the repository interface. This will add the CRUD operations to it.
   
   
## Using CRUD Operations

### Create Entity

``DemoApplication``:

1. Create the following customers and persist them in the database
    - *Bob Builder* from *New York*
    - *Sue Smart* from *Vancouver*
    - *Ann Analyzer* from *London*
    - *Lin Li* from *Beijing*
    - *Carlos Calculador* from *Madrid*
    
1. Print their values on the console. Check, if they got correct IDs.


### Find Entity by ID

``DemoApplication``:

1. Find the customer with the ID '3'.

1. Print the customer on the console.


### Find Entity by Name

``DemoApplication``:

1. Find the customer with the name 'Sue Smart'.

1. Print the customer on the console.


### Find All Entities

``DemoApplication``:

1. Find all customers.

1. Print the customers on the console.


### Update an Entity

``DemoApplication``:

1. Find the customer with ID '5'.

1. Change the city of the customer to 'Barcelona'.

1. Find the customer with ID '5' again.

1. Print the customer on the console.


### Delete an Entity

``DemoApplication``:

1. Delete the customers with ID '2' and '4'.

1. Print all remaining customers on the console.
