package demo.spring.data.rest.client;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.spring.data.jdbc.customer.Customer;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
public class RestClientApplication {
	
	// methods /////

	public static void main(final String[] args) {
		
		new RestClientApplication().runDemo();
		
	}
	
	private void runDemo() {
		
		System.out.println("Running REST client ...");
		
		
		System.out.println("A) Creating customer ...");
		
		// TODO
		//
		//  o Use the 'RestTemplate' to create a new customer. The new user
		//    will get the ID 15. Since it is not written back to the entity by
		//    default, we will have to set it hard-coded.
		//
		//  o Print the customer with the ID 15.
		

		System.out.println("B) Updating customer ...");
		
		// TODO
		//
		//  o Read the customer with the ID 15. Change the city of the address
		//    to some other city.
		//
		//  o Update the customer with the 'RestTemplate'.
		//
		//  o Print the customer with the ID 15.
		
		
		System.out.println("C) Removing customer ...");
		
		// TODO
		//
		//  o Delete the customer with the ID 15.
		//
		//  o Verify that the customer has been deleted.

		
		System.out.println("Finished.");
			
	}
	
	private RestTemplate restTemplate() {
		
		return new RestTemplate();
		
	}
	
	private String url() {
		
		return "http://localhost:8080/customers";
		
	}
	
	private Customer getCustomer(final int id) {
		
		// TODO
		//
		//  o Use the 'RestTemplate' to read the customer with
		//    the specified ID.

		return null;
			
	}
	
	private void printCustomer(final int id) {
		
		try {
			
			System.out.println(this.getCustomer(id).toString());
			
		} // try
		catch (RestClientException e) {
			
			System.out.println(e.getMessage());
			
		} // catch

	}
	
}
