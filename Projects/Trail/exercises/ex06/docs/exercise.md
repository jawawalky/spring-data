# REST

If we include the *spring-boot-starter-data-rest* of *Spring Boot*, then
the methods of our repository can be published as *restful* WebService methods.
We can try to call some of the methods via the browser or some tool that allows
*HTTP* requests, such as *RESTED* or *Postman*.

> The service is published under `http://localhost:8080/customers`.

- `POST + http://localhost:8080/customers + Customer` creates a new `Customer`
in the database.
- `GET + http://localhost:8080/customers/15` reads the `Customer` with ID *15*.
- `PUT + http://localhost:8080/customers/15 + Customer` updates the `Customer`
with ID *15*.
- `DELETE + http://localhost:8080/customers/15` deletes the `Customer` with
ID *15*.

All customers are returned by

- `GET + http://localhost:8080/customers`

in the database.
## Calling Restful WebServices

Open the class
[RestClientApplication](../src/main/java/demo/spring/data/rest/client/RestClientApplication.java).

Follow the *TODO*s and complete the code.
