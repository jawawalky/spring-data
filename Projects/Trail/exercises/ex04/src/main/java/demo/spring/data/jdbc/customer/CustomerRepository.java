package demo.spring.data.jdbc.customer;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries with dynamic.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> getByName(String name);
	
	// TODO
	//
	//  o Create a query method with a name pattern and the possibility for
	//    sorting.
	//
	//  o Create the general query method with the possibility for paging.
	//
	//    Hint: That is a method with a pre-defined name.
	//
	
}
