package demo.spring.data.jdbc;

import static java.time.Month.APRIL;
import static java.time.Month.AUGUST;
import static java.time.Month.DECEMBER;
import static java.time.Month.FEBRUARY;
import static java.time.Month.JULY;
import static java.time.Month.MARCH;
import static java.time.Month.NOVEMBER;
import static java.time.Month.OCTOBER;
import static java.time.Month.SEPTEMBER;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.address.Address;
import demo.spring.data.jdbc.customer.Customer;
import demo.spring.data.jdbc.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		this.populateDatabase();
		
		
		// Sort Customers by Name /////
		
		System.out.println("A) Sorting customers ascending by name ...");
		
		// TODO
		//
		//  o Read all customers, whose name starts with 'J' and sort them
		//    by their names in ascending order.
		//
		//  o Print the results on the console.
		//

		
		// Sort Customers by Date-of-Birth /////
		
		System.out.println("B) Sorting customers descending by date-of-birth ...");
		
		// TODO
		//
		//  o Read all customers, whose name starts with 'J' and sort them
		//    by their birth dates in descending order.
		//
		//  o Print the results on the console.
		//

		
		// Sort Customers by Name and by Date-of-Birth /////
		
		System.out.println("C) Sorting customers by name and date-of-birth ...");
		
		// TODO
		//
		//  o Read all customers, whose name starts with 'James' and sort them
		//    primarily ascending by their names and secondarily descending
		//    by their birth date.
		//
		//  o Print the results on the console.
		//

		
		// Page Through Customers /////
		
		System.out.println("D) Paging through customers ...");
		
		System.out.println("> First Page:");
		
		// TODO
		//
		//  o Read the first five customers and print them on the console.
		//

		System.out.println("> Second Page:");
		
		// TODO
		//
		//  o Read the second five customers and print them on the console.
		//

		
		// Page with Pageable Object /////
		
		System.out.println("E) Paging with Pageable object ...");
		
		System.out.println("> First Page:");
		
		// TODO
		//
		//  o Create a 'Pageable' object for the first five customers.
		//
		//  o Read them from the database and print them on the console.
		//
		

		System.out.println("> Second Page:");
		
		// TODO
		//
		//  o Move the 'Pageable' object to the next page.
		//
		//  o Use it to read the next five customers from the database and
		//    print them, too.
		//

		
		// Page and Sort /////
		
		System.out.println("F) Paging and sorting customers ...");
		
		// TODO
		//
		//  o Read the first four customers and sort them by their names.
		//
		
	}
	
	private void populateDatabase() {
		
		this.customerRepository.save(
			new Customer(
				"Bob Builder",
				LocalDate.of(1980, NOVEMBER, 7),
				new Address("5th Avenue", "28438", "New York")
			)
		);
		
		this.customerRepository.save(
			new Customer(
				"Sue Smart",
				LocalDate.of(1999, MARCH, 12),
				new Address("Snow Drive 87", "77399", "Vancouver")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Ann Analyzer",
				LocalDate.of(1991, JULY, 8),
				new Address("Oxford Road 34", "11MW BC23", "London")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Lin Li",
				LocalDate.of(2001, NOVEMBER, 19),
				new Address("Nang Gin Raod 227", "110 218", "Beijing")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Carlos Calculador",
				LocalDate.of(1993, AUGUST, 30),
				new Address("Calle del Sol", "21002", "Madrid")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Jimmy Speed",
				LocalDate.of(1997, JULY, 10),
				new Address("Baker Street 221b", "12SW GT88", "London")
			)
		);
				
		this.customerRepository.save(
			new Customer(
				"Joe Busy",
				LocalDate.of(1972, APRIL, 1),
				new Address("Silk Road", "73329", "Boston")
			)
		);
					
		this.customerRepository.save(
			new Customer(
				"James Bond",
				LocalDate.of(1925, SEPTEMBER, 13),
				new Address("Action Street 007", "12MW SE45", "Edinburgh")
			)
		);
						
		this.customerRepository.save(
			new Customer(
				"James Blond",
				LocalDate.of(1975, DECEMBER, 18),
				new Address("Winter Avenue 2", "36273", "Sidney")
			)
		);
							
		this.customerRepository.save(
			new Customer(
				"Jeffrey Archer",
				LocalDate.of(1988, MARCH, 26),
				new Address("Summer Street 83", "1928", "Auckland")
			)
		);
								
		this.customerRepository.save(
			new Customer(
				"James Bond",
				LocalDate.of(1985, SEPTEMBER, 13),
				new Address("Action Street 007", "12MW SE45", "Edinburgh")
			)
		);
								
		this.customerRepository.save(
			new Customer(
				"Juan Ferrer",
				LocalDate.of(1983, FEBRUARY, 16),
				new Address("Malecon 129", "1119", "Havana")
			)
		);
									
		this.customerRepository.save(
			new Customer(
				"Jose Blanco",
				LocalDate.of(2001, OCTOBER, 7),
				new Address("Calle de Flores 12", "1008", "San Juan")
			)
		);
										
		this.customerRepository.save(
			new Customer(
				"James Bond",
				LocalDate.of(1955, SEPTEMBER, 13),
				new Address("Action Street 007", "12MW SE45", "Edinburgh")
			)
		);
							
	}

}
