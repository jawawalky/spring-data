# Spring Data - Sorting

## Declaring Sortability

`CustomerRepository`:

1. Create a query method with a name pattern and the possibility for sorting.


## Sorting Data

`DemoApplication`

*Sort Ascending*

1. Read all customers, whose name starts with *J* and sort them
   by their names in ascending order.
1. Print the results on the console.

*Sort Descending*

1. Read all customers, whose name starts with *J* and sort them
   by their birth dates in descending order.
1. Print the results on the console.

*Combining Sorts*

1. Read all customers, whose name starts with *James* and sort them
   primarily ascending by their names and secondarily descending
   by their birth date.
1. Print the results on the console.


# Spring Data - Paging

## Declaring Paging

`CustomerRepository`:

1. Create the general query method with the possibility for paging.

   **Hint:** That is a method with a pre-defined name.


## Paging Data

`DemoApplication`

*Simple Paging*

1. Read the first five customers and print them on the console.
1. Read the second five customers and print them on the console.

*Using Pageable Object*

1. Create a 'Pageable' object for the first five customers.
1. Read them from the database and print them on the console.
1. Move the 'Pageable' object to the next page.
1. Use it to read the next five customers from the database and
   print them, too.
   
*Page and Sort*

1. Read the first four customers and sort them by their names.
