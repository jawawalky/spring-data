# Spring Data - Projections

Up to now we have seen queries, which return the full domain object.
Often only a few properties of the domain object are needed. So fetching
and transporting the entire domain object is unnecessary and expensive.
That's where projections jump in. Projections only fetch a subset of
the domain object's properties.

## Interface-based Projections

One way to define a projection in *Spring Data*, is to define an interface,
which only contains the properties that we want.

### Closed Projections

A projection is considered to be closed, when directly refers to properties
of the backing domain object.

**Example**

Consider the following domain object

```
public class Airport {

    @Id
    private String code;
    private String name;
    private String city;
    
    public String getCode() { return this.code; }
    public void setCode(final String code) { this.code = code; }

    public String getName() { return this.name; }
    public void setName(final String name) { this.name = name; }

    public String getCity() { return this.city; }
    public void setCity(final String city) { this.city = city; }

}
```

We can define a simple, closed projection onto `Airport` objects with
the following interface

```
public interface AirportRef {

    String getCode();
    String getName();

}
```

In the repository interface we define the query, which produces all airport
references

```
public interface AirportRepository implements Repository<Airport, String> {

    List<AirportRef> findBy();
	
}
```

Since the elements of the result list are `AirportRef` objects,
*Spring Data* will automatically perform a query, which only contains
the property values

- `code` and
- `name`

> *Spring Data* can optimize closed projections.

Of course we can define queries with condition, as we learned earlier. Let's
say, we wanted to find an `Airport` with a certain code, then the query method
would be

```
public interface AirportRepository implements Repository<Airport, String> {

    AirportRef getByCode(String code);
	
}
```

### Nested Projections

*Spring Data* allows us to define nested projections. If our domain model is
complex, then we have several domain types and there are relations between
the domain objects. Let's say we had a domain object, which is the parent of
some other domain object. The projection to that domain object should also
contain some data of the child object, then we could define a projection on
the type of the child object and retrieve it together with the projection of
the parent object.

**Example**

Here is our already known domain type

```
public class Airport {

    @Id
    private String code;
    private String name;
    private String city;
    
    // getter- and setter-methods
    
}
```

and here another domain type

```
public class Airline {

    @Id
    private String  code;
    private String  name;
    private Airport homeBase;
    
    // getter- and setter-methods
    
}
```

An object of type `Airline` refers to an object of type `Airport` as its
`homeBase`. If we wanted to retrieve the name of an airline together with
the city of its home base, we could define the following projection interfaces

```
public interface AirportCity {

    String getCity();
    
}
```

and

```
public interface AirlineHome {

    String getName();
    AirportCity getHomeBase();
    
}
```

The query in the repository would look like this

```
public interface AirlineRepository implements Repository<Airline, String> {

    AirlineHome getByCode(String code);
	
}
```

In the code, where we get the airline information, we can access the name of
the airline and the city of its home base.

```
@Component
public class MyComponent {

    @Autowired
    private AirlineRepository airlineRepository;
    
    someMethod() {
    
        const airlineHome = this.airlineRepository.getByCode("OS");
        console.log('Name: ' + airlineHome.getName());
        console.log('City: ' + airlineHome.getHomeBase().getCity());
        
    }
    
}
```

Optionally we could also define the projection to the child object as
an embedded interface of the parent projection interface.

```
public interface AirlineHome {

    public interface AirportCity {

        String getCity();
    
    }
    
    String getName();
    AirportCity getHomeBase();
    
}
```

### Open Projections

Closed and nested projections already allow us a wide range of projections.
But there are still some limitations, such as

- values composed of different projection values,
- values with other names than the properties names,
- etc.

These limitations can be abrogated by open projections.

#### Spring Expression Language

In open projections, we can use the *Spring Expression Language* (= *SpEL*)
together with the `@Value` annotation. The *SpEL* allows us to access
the query results and compose data as we want.

**Example**

The result objects can be accessed by `#{target}` in a *SpEL* expression

```
public interface AirlineTag {

    @Value("#{target.name} + ' based in ' + #{target.homeBase.city}")
    String getLabel();
    
}
```

#### Default Methods

Since *Java 8* we can use default methods in interfaces. That allows us
to compose data from projected properties

**Example**

The label is composed of the properties `code` and `name`

```
public interface AirlineTag {

    String getCode();
    
    String getName();

    default String getLabel() {
    
        return this.getCode() + " - " + this.getName();
        
    }
    
}
```

#### Spring Beans

It is possible to use *Spring Beans* on open projections. The bean defines
some mapping on the domain object. Then it can be used in a *SpEL* expression
to create the projection value.

**Example**

The *Spring Bean*, which provides the mapping

```
@Component
public class TagMapping {

    public String getLabel(Airline airline) {
    
        return airline.getCode() + " - " + airline.getName();
        
    }
    
}
```

The bean is referenced in the expression by its bean name. In this case
that would be `@tagMapping`.

```
public interface AirlineTag {

    @Value("#{@tagMapping.getLabel(target)}")
    String getLabel();
    
}
```

#### Projections with Arguments

We can even add variable arguments to our projections. We pass them to
the projection method. In the *SpEL* expression we reference them by
`args[...]` with the appropriate index.

**Example**

```
public interface AirlineTag {

    @Value("args[0] + #{target.name}" + args[1])
    String getLabel(String prefix, String suffix);
    
}
```

### Class-based Projections

Often a *JavaBean*/*POJO*, representing a cutout of the domain object, is used
for the transport of data from the server to a client and vice versa. Such
objects are called *Data Transfer Object* (short: *DTO*).

*Spring Data* supports the projection to *DTO*s in the same way as projections
to interfaces. So if we want to fill some *DTO* with the data of a domain
object, then we can do it just in the same way as we do it with interfaces.

Let's say we wanted to transport the `code` and the `name` of an `Airline`,
then we could define an `AirlineDTO` in the following way

```
public class AirlineDTO {

    private String  code;
    private String  name;
    
    // getter- and setter-methods
    
}
```

Our repository would declare a projection to the *DTO* in the same way
as we did it for our projection interfaces.

```
public interface AirlineRepository implements Repository<Airline, String> {

    AirlineDTO getByCode(String code);
	
}
```

## Dynamic Projections

If we wanted to perform the same query, but with different projections as
result, it would be rather awkward and cumbersome, if we had to define
a separate query method for each projection that we wanted to use. Luckily
*Spring Data* allows us to define *dynamic projections*. A *dynamic projection*
allows us to make a query method generic and pass the class definition of
the projection type (either interface or class), as the last parameter of
the query method.

Let's have a look at out query, which produces all airports

```
public interface AirportRepository implements Repository<Airport, String> {

    List<AirportRef> findBy();
	
    AirportRef getByCode(String code);
	
}
```

If we replace `AirportRef` by a generic type `T` and pass the class definition
of `T` to the query method, then we can use the query with any projection type.

```
public interface AirportRepository implements Repository<Airport, String> {

    <T> List<T> findBy(Class<T> projection);
	
    T getByCode(String code, Class<T> projection);
	
}
```

