# Spring Data - Projections

## Closed Projection

Consider a closed projection, which contains

- the ID and
- the name

of a `Customer`.

Create an appropriate interface `CustomerRef` and add a method
`getByName(...)` to
the [CustomerRepository](../src/main/java/demo/spring/data/jdbc/customer/CustomerRepository.java), which retrieves a `CustomerRef` with
the data of the correct customer.

In the section *A)* of
[DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java)
retrieve the data of some customer, using
the `getByName(...)` method and print the customer data on the console.

## Dynamic Projections

Using dynamic projections make our queries more flexible. They can be used
with any kind of projection.

Define a query method `findBy(...)` in
[CustomerRepository](../src/main/java/demo/spring/data/jdbc/customer/CustomerRepository.java),
which retrieves all available customers and can be used with any projection.

In the section *B)* of
[DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java)
retrieve `CustomerRef`s for all customers, and print them on the console.

## Nested Projections

Define a projection to a `Customer`, which contains the following data

- name of the customer and
- a label with the address data (street, zip-code and city).

Use a pair of nested interfaces. One for the `Customer` data and one for
the `Address` data. Call the interface for the customer data `CustomerTag`
and that for the address data `AddressTag`.

In the section *C)d)* of
[DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java)
print a `CustomerTag` for an arbitrary customer.

## Open Projections

Write an interface `LongTag` for a `Customer` domain object, which defines
a projection to a string with the following format

```
<name> <address.street>, <address.zipCode> <address.city>
```

Use `@Value` and an appropriate *SpEL* expression.

In the section *C)a)* of
[DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java)
print a `LongTag` for an arbitrary customer.

Now write an interface `ShortTag` for a `Customer` domain object, which defines
a projection to a string with the following format

```
<name> from <address.city>
```

Define projections for `name`and `address.city`. Create a `default` method,
which composes the label from the other two projections.

In the section *C)b)* of
[DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java)
print a `ShortTag` for an arbitrary customer.

## Data Transfer Objects

Define a class `CustomerDTO` with the following properties

- `id`
- `name`
- `dateOfBirth`

In the section *D)* of
[DemoApplication](../src/main/java/demo/spring/data/jdbc/DemoApplication.java)
query a customer and return his/her data as a *DTO*.

Print the *DTO* data on the console.
