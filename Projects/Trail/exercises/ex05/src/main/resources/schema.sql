CREATE TABLE Customer (
    id            INTEGER IDENTITY,
    name          VARCHAR(50) NOT NULL,
    date_of_birth DATE
);

CREATE TABLE Address (
    id        INTEGER IDENTITY,
    street    VARCHAR(50),
    zip_code  VARCHAR(10),
    city      VARCHAR(50),
    customer  INTEGER,
    FOREIGN KEY (customer) REFERENCES Customer(id)
);
