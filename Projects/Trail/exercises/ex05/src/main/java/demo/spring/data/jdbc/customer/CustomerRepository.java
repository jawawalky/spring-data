package demo.spring.data.jdbc.customer;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries with different types of projections.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	// TODO
	//
	//  o Get a customer by his/her name, returning a 'CustomerRef' object.
	
	// TODO
	//
	//  o Write a query method 'findBy(...)', which retrieves all customers
	//    for any projection.
	
	// TODO
	//
	//  o Write a query method 'queryById(...)', which retrieves the customer
	//    with a certain ID. The method should work for any projection.

}
