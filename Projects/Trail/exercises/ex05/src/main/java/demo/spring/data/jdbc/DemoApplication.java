package demo.spring.data.jdbc;

import static java.time.Month.APRIL;
import static java.time.Month.AUGUST;
import static java.time.Month.JULY;
import static java.time.Month.MARCH;
import static java.time.Month.NOVEMBER;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jdbc.address.Address;
import demo.spring.data.jdbc.customer.Customer;
import demo.spring.data.jdbc.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		this.populateDatabase();
		
		
		// Closed Projections /////
		
		System.out.println("A) Closed Projections ...");
		
		// TODO
		//
		//  o Get a customer by his/her name by using a closed projection
		//    to 'CustomerRef'.
		//
		//  o Print the projection data on the console.
		

		// Dynamic Projections /////
		
		System.out.println("B) Dynamic Projections ...");
		
		// TODO
		//
		//  o Find all customers using the projection 'CustomerRef' and
		//    print them on the console.
		
		
		// Open Projections /////
		
		System.out.println("C) Open Projections ...");
		
		// TODO C)a)
		// 
		//  o Get a 'LongTag' for one customer and print it on the console.
		
		System.out.println("a) ");

		// TODO C)b)
		// 
		//  o Get a 'ShortTag' for one customer and print it on the console.
		
		System.out.println("b) ");

		// TODO C)d)
		// 
		//  o Get a 'CustomerTag' for one customer and print it on the console.
		
		System.out.println("d) ");

		
		// DTO Projections /////
		
		System.out.println("D) DTO Projections ...");
		
		// TODO
		//
		//  o Query some customer and return his/her data as a 'CurtomerDTO'.
		
	}
	
	private void populateDatabase() {
		
		this.customerRepository.save(
			new Customer(
				"Bob Builder",
				LocalDate.of(1980, NOVEMBER, 7),
				new Address("5th Avenue", "28438", "New York")
			)
		);
		
		this.customerRepository.save(
			new Customer(
				"Sue Smart",
				LocalDate.of(1999, MARCH, 12),
				new Address("Snow Drive 87", "77399", "Vancouver")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Ann Analyzer",
				LocalDate.of(1991, JULY, 8),
				new Address("Oxford Road 34", "11MW BC23", "London")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Lin Li",
				LocalDate.of(2001, NOVEMBER, 19),
				new Address("Nang Gin Raod 227", "110 218", "Beijing")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Carlos Calculador",
				LocalDate.of(1993, AUGUST, 30),
				new Address("Calle del Sol", "21002", "Madrid")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Jimmy Speed",
				LocalDate.of(1997, JULY, 10),
				new Address("Baker Street 221b", "12SW GT88", "London")
			)
		);
				
		this.customerRepository.save(
			new Customer(
				"Joe Busy",
				LocalDate.of(1972, APRIL, 1),
				new Address("Silk Road", "73329", "Boston")
			)
		);
					
	}

}
