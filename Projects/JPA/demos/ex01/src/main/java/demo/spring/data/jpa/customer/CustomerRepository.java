package demo.spring.data.jpa.customer;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Queries with dynamic.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> getByName(String name);
	
}
