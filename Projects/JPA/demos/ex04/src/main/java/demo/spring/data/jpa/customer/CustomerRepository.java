package demo.spring.data.jpa.customer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Query methods with '@Query' annotations.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	@Query("SELECT c FROM Customer c WHERE c.name = ?1")
	Optional<Customer> findByName(String name);
	
	@Query("SELECT c FROM Customer c WHERE c.address.city = ?1")
	List<Customer> whoLivesIn(String city);
	
}
