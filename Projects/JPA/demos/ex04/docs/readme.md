# Spring Data JPA - Query Annotation

Queries can be defined directly on query methods of the repository interface
with the `@Query` annotation. The *JPQL*-query string is the value of
the `@Query` annotation.


## Defining a Query

- Write a query method in the repository interface.
- Add the annotation `@Query` with the desired *JPQL*-query.


**Example**

```
import org.springframework.data.jpa.repository.Query;
...

public interface ContactRepository extends CrudRepository<Contact, Long> {
  
  @Query("SELECT c FROM Contact c WHERE c.name = ?1")
  Optional<Contact> findByName(String name);
  
}
```
