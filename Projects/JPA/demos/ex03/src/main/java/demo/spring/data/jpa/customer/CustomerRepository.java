package demo.spring.data.jpa.customer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Query methods, which match named queries.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> findByName(String name);  // <- Due to its name, the query method refers to
	                                             //    a named JPA query called 'Customer.findByName'.
	                                             //    The parameter replaces the first place holder
	                                             //    '?1' of the query.
	                                             //
	                                             //    Query: ('SELECT c FROM Customer c WHERE c.name = ?1')
	
	List<Customer> whoLivesIn(String city);
	
}
