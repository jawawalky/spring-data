package demo.spring.data.jpa;

import static java.time.Month.JUNE;
import static java.time.Month.NOVEMBER;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jpa.address.Address;
import demo.spring.data.jpa.customer.Customer;
import demo.spring.data.jpa.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		// Persist a new customer /////
		
		System.out.println("A) Persisting customer ...");
		
		Customer bob =
			new Customer(
				1L,
				"Bob Builder",
				LocalDate.of(1980, NOVEMBER, 7),
				new Address("5th Avenue", "28438", "New York")
			);
		this.customerRepository.save(bob);
		
		Customer customer = this.customerRepository.getByName("Bob Builder").get();
		this.print(customer);
		
		
		// Update customer /////

		System.out.println("B) Updating customer ...");
		
		customer.setDateOfBirth(LocalDate.of(2005, JUNE, 19));
		this.customerRepository.save(customer);
		
		customer = this.customerRepository.getByName("Bob Builder").get();
		this.print(customer);
		
	}
	
	private void print(final Customer customer) {
		
		System.out.printf(
			"[%d] %s (%s) lives in %s",
			customer.getId(),
			customer.getName(),
			customer.getDateOfBirth().format(DateTimeFormatter.ISO_LOCAL_DATE),
			customer.getAddress().toString()
		);
		System.out.println();
		
	}
	
}
