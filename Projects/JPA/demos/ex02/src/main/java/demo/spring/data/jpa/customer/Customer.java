package demo.spring.data.jpa.customer;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

import demo.spring.data.jpa.AbstractEntity;
import demo.spring.data.jpa.address.Address;

/**
 * <h1>A Customer Entity</h1>
 * 
 * @author Franz Tost
 *
 */
@Entity
public class Customer extends AbstractEntity<Long> {
	
	// fields /////
	
	@Id                                // <- The field contains the ID, but its
	private Long id;                   //    value is not generated. It will be
	                                   //    provided manually.
	
	@Version
	private Integer version;
	
	@Column(length = 50)
	private String name;
	
	private LocalDate dateOfBirth;
	
	@Embedded
	private Address address;
	
	
	// constructors /////
	
	public Customer() { }
	
	public Customer(
		final Long      id,            // <- The ID is passed to the constructor.
		final String    name,
		final LocalDate dateOfBirth,
		final Address   address
	) {
		
		this();
		
		this.id          = id;
		this.name        = name;
		this.dateOfBirth = dateOfBirth;
		this.address     = address;
		
	}

	
	// methods /////
	
	@Override
	public Long      getId()                                     { return this.id;                 }
	public void      setId(final Long id)                        { this.id = id;                   }

	public Integer   getVersion()                                { return this.version;            }
	public void      setVersion(final Integer version)           { this.version = version;         }

	public String    getName()                                   { return this.name;               }
	public void      setName(final String name)                  { this.name = name;               }

	public LocalDate getDateOfBirth()                            { return this.dateOfBirth;        }
	public void      setDateOfBirth(final LocalDate dateOfBirth) { this.dateOfBirth = dateOfBirth; }

	public Address   getAddress()                                { return this.address;            }
	public void      setAddress(final Address address)           { this.address = address;         }


	@Override public String toString() {
		
		return this.getName();
		
	}

}
