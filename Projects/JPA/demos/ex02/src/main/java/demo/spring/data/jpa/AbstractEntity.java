package demo.spring.data.jpa;

import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import org.springframework.data.domain.Persistable;

/**
 * A base class for entities, which set their ID manually.
 * 
 * @author Franz Tost
 *
 * @param <ID> the type of the primary key.
 */
@MappedSuperclass                                     // <- Marks this class as
public abstract class AbstractEntity<ID>              //    a base class for entities.
	implements
		Persistable<ID>                               // <- Provides support for
{                                                     //    persistent state recognition
	                                                  //    of the entity.
	// fields /////
	
	@Transient private boolean persistent = false;    // <- A flag, which stores
	                                                  //    internally, if the entity
	                                                  //    is persistent or not.
	
	// methods /////

	@Override
	public boolean isNew() {                          // <- Tells Spring Data, if
		return !this.persistent;                      //    the entity is new or, if it
	 }                                                //    already exists.

	@PrePersist @PostLoad                             // <- A life-cycle method, which
	void persistent() { this.persistent = true; }     //    sets the internal flag for
	                                                  //    the persistence state.

}
