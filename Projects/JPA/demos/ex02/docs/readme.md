# Spring Data JPA - Persistable

If we have entities, which haven't got an ID that is generated, then we can
use the `Persistable<ID>` interface to support persistence. The `Persistable`
interface requires a method `getId()`, which tells *Spring Data* the ID value
of the entity. And it declares a method `boolean isNew()`, which tells
*Spring Data*, if the entity is new or if it already exists in the database.

We can manage that information by adding a transient `boolean` attribute.
The value of the attribute is managed by life-cycle methods of the entity.
By default the *persistent* attribute is `false`. When the entity is persisted,
then it is set to `true`. It is also set to `true` when the entity is loaded
from the database.


## An Example

```
@Entity
public class Contact implements Persistable<Long> {

  @Id
  private Long id;
  
  private String name;
  
  @Transient
  private boolean persistent = false;
  
  public Contact() { }
  public Contact(Long id, String name) {
    this.id   = id;
    this.name = name;
  }

  @Override public boolean isNew() {
    return !this.persistent;
  }
  
  @PrePersist @PostLoad void persistent() {
    this.persistent = true;
  }
  
  @Override
  public Long   getId()                    { return this.id;   }
  public void   setId(final Long id)       { this.id = id;     }
  
  public String getName()                  { return this.name; }
  public void   setName(final String name) { this.name = name; }

}
```
