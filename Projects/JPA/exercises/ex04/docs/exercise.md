# Spring Data JPA - Query Annotation

1. Instead of using named *JPA* queries, we would like to use queries defined
   by the `@Query` annotation.
1. Refactor the application, so the named queries are replaced by queries
   defined with `@Query`.
   
Hint: You only need to edit the two files

- [Customer](./src/main/java/demo/spring/data/jpa/customer/Customer.java)
- [CustomerRepository](./src/main/java/demo/spring/data/jpa/customer/CustomerRepository.java)
