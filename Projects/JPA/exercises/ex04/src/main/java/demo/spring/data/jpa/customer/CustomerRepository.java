package demo.spring.data.jpa.customer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Query methods, which match named queries.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	Optional<Customer> findByName(String name);
	
	List<Customer> whoLivesIn(String city);
	
}
