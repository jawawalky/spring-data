# Spring Data JPA - Storing Entity

## The Entity Classes

[Address](./src/main/java/demo/spring/data/jpa/address/Address.java)

1. Let `Address` be an embeddable JPA element.
1. Add JPA configurations as you think to the properties of this class.
   
   
[Customer](./src/main/java/demo/spring/data/jpa/customer/Customer.java)

1. Convert this class into a JPA entity class.
1. The ID should be a generated value.
1. The property `version` should be the version count for optimistic locking.
1. The address should be embedded into this entity.


## CRUD Repository

[CustomerRepository](./src/main/java/demo/spring/data/jpa/customer/CustomerRepository.java)

1. Define the repository as a `CrudRepository` or add the `save(...)` method
   of the `CrudRepository` to your repository.
   
   
## Persisting Entities

[DemoApplication](./src/main/java/demo/spring/data/jpa/DemoApplication.java)

*Persisting Customer*

1. Persist customer *Bob*.
1. Read Bob from the database.
1. Print the read customer on the console.

   Hint: Use the method 'print(Customer)'.
   
   
*Update Customer*

1. Change the birth date of the customer, you just read from the database.
1. Update the customer in the database.
1. Read 'Bob' again.
1. Print his data on the console.
   
   Hint: Use the method 'print(Customer)'.
