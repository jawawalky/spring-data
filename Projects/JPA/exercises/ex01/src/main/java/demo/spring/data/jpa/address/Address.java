package demo.spring.data.jpa.address;

/**
 * <h1>An Address Entity</h1>
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let 'Address' be an embeddable JPA element.
 *  
 *  o Add JPA configurations as you think to the properties of this class.
 */
public class Address {
	
	// fields /////
	
	private String street;
	
	private String zipCode;
	
	private String city;
	
	
	// constructors /////
	
	public Address() { }
	
	public Address(
		final String street,
		final String zipCode,
		final String city
	) {
		
		this();
		
		this.street  = street;
		this.zipCode = zipCode;
		this.city    = city;
		
	}

	
	// methods /////
	
	public String getStreet()                      { return this.street;     }
	public void   setStreet(final String street)   { this.street = street;   }

	public String getZipCode()                     { return this.zipCode;    }
	public void   setZipCode(final String zipCode) { this.zipCode = zipCode; }

	public String getCity()                        { return this.city;       }
	public void   setCity(final String city)       { this.city = city;       }


	@Override public String toString() {
		
		return String.format(
			"%s, %s %s",
			this.getStreet(),
			this.getZipCode(),
			this.getCity()
		);
		
	}

}
