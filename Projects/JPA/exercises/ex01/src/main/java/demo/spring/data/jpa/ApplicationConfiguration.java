package demo.spring.data.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * <h1>The Configuration Class</h1>
 * 
 * The configuration is responsible for providing required bean instances and
 * bean proxies.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableJpaRepositories                      // <- Triggers the generation
public class ApplicationConfiguration { }   //    of JPA proxies.
