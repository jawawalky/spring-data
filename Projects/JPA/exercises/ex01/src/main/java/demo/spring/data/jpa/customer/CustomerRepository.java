package demo.spring.data.jpa.customer;

import java.util.Optional;

import org.springframework.data.repository.Repository;

/**
 * <h1>The Repository Class</h1>
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Make this repository a CRUD repository.
 */
public interface CustomerRepository extends Repository<Customer, Long> {

	// methods /////
	
	Optional<Customer> getByName(String name);
	
}
