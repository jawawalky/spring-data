package demo.spring.data.jpa.address;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * <h1>An Address Entity</h1>
 * 
 * @author Franz Tost
 *
 */
@Embeddable
public class Address {
	
	// fields /////
	
	@Column(length = 100)
	private String street;
	
	@Column(length = 10)
	private String zipCode;
	
	@Column(length = 50)
	private String city;
	
	
	// constructors /////
	
	public Address() { }
	
	public Address(
		final String street,
		final String zipCode,
		final String city
	) {
		
		this();
		
		this.street  = street;
		this.zipCode = zipCode;
		this.city    = city;
		
	}

	
	// methods /////
	
	public String getStreet()                      { return this.street;     }
	public void   setStreet(final String street)   { this.street = street;   }

	public String getZipCode()                     { return this.zipCode;    }
	public void   setZipCode(final String zipCode) { this.zipCode = zipCode; }

	public String getCity()                        { return this.city;       }
	public void   setCity(final String city)       { this.city = city;       }


	@Override public String toString() {
		
		return String.format(
			"%s, %s %s",
			this.getStreet(),
			this.getZipCode(),
			this.getCity()
		);
		
	}

}
