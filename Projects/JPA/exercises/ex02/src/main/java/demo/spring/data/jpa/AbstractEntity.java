package demo.spring.data.jpa;

import javax.persistence.MappedSuperclass;

/**
 * A base class for entities, which set their ID manually.
 * 
 * @author Franz Tost
 *
 * @param <ID> the type of the primary key.
 */
@MappedSuperclass
public abstract class AbstractEntity<ID> {
	
	// TODO
	//
	//  o Let this class be a 'Persistable' entity base class.
	//
	//    Hint: Use the interface 'Persistable'.
	//
	//  o Manage an internal state, which tells Spring Data, if the entity
	//    is new or if it is persistent.
	//
	//    Hints:
	//
	//      o Use an internal boolean flag.
	//      o Manage that flag with according entity life-cycle methods.
	//
	
}
