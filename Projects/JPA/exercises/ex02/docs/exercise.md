# Spring Data JPA - Persistable

In this exercise we will create a base class for entities, which have an ID
that will be set manually.


## The Entity Base Class

[AbstractEntity](./src/main/java/demo/spring/data/jpa/AbstractEntity.java)

1. Let this class be a persistable entity base class.

   Hint: Use the interface `Persistable`.
   
1. Manage an internal state, which tells Spring Data, if the entity
   is new or if it is persistent.
   
   Hints:
     - Use an internal boolean flag.
     - Manage that flag with according entity life-cycle methods.

     
## The Entity Class

[Customer](./src/main/java/demo/spring/data/jpa/Customer.java)

1. Extend the `AbstractEntity` class.

