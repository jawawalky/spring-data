# Spring Data JPA - Named Queries

With *JPA* it is possible to define named queries. A named query is a query,
which can be identified by a name. If we define a query method with the name
of the query in the repository interface, then we can execute the query
by calling the query method of the repository interface.


## Defining a Named JPA Query

- Typically named queries are defined in the entity class, whose entities they
  produce.
- The name of a query must adhere to the pattern
  `<simple-entity-class-name>.<query-method-name>`.
- Parameters of the query must be provided by parameters of the query method.


**Example**

```
import javax.persistence.NamedQuery;
...

@NamedQuery(
	name  = "Contact.findByName",
	query = "SELECT c FROM Contact c WHERE c.name = ?1"
)
@Entity    
public class Contact { ... }
```

## Defining the Query Method

- The repository interface for the entity class must have a method with
  a name, which corresponds to the part after the dot of the query name.
- The query method must have a parameter for each place holder in the query.
- The order of the parameters corresponds to the number of the place holder.

**Example**

```
public interface ContactRepository extends CrudRepository<Contact, Long> {
  
  Optional<Contact> findByName(String name);
  
}
```
