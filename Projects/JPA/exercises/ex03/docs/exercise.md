# Spring Data JPA - Named Queries


## Defining Named JPA Queries

[Customer](./src/main/java/demo/spring/data/jpa/customer/Customer.java)

1. Define a named query, which finds a customer by his/her name.
1. Define a named query, which finds all customers, who live in the same city.


## Defining Query Methods

[CustomerRepository](./src/main/java/demo/spring/data/jpa/customer/CustomerRepository.java)

1. Define appropriate query methods for the two named queries that
   you defined in the 'Customer' class.
   
   
## Executing the Queries

[DemoApplication](./src/main/java/demo/spring/data/jpa/DemoApplication.java)

1. Find 'Bob Builder' and print his data on the console.
1. Find all customers, who live in New York, and print them on the console.
