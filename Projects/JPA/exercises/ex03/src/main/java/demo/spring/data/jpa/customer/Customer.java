package demo.spring.data.jpa.customer;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import demo.spring.data.jpa.address.Address;

/**
 * <h1>A Customer Entity</h1>
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Define a named query, which finds a customer by his/her name.
 *  
 *  o Define a named query, which finds all customers, who live in
 *    the same city.
 */
@Entity    
public class Customer {
	
	// fields /////
	
	@Id @GeneratedValue
	private Long id;
	
	@Version
	private Integer version;
	
	@Column(length = 50)
	private String name;
	
	private LocalDate dateOfBirth;
	
	@Embedded
	private Address address;
	
	
	// constructors /////
	
	public Customer() { }
	
	public Customer(
		final String    name,
		final LocalDate dateOfBirth,
		final Address   address
	) {
		
		this();
		
		this.name        = name;
		this.dateOfBirth = dateOfBirth;
		this.address     = address;
		
	}

	
	// methods /////
	
	public Long      getId()                                     { return this.id;                 }
	public void      setId(final Long id)                        { this.id = id;                   }

	public Integer   getVersion()                                { return this.version;            }
	public void      setVersion(final Integer version)           { this.version = version;         }

	public String    getName()                                   { return this.name;               }
	public void      setName(final String name)                  { this.name = name;               }

	public LocalDate getDateOfBirth()                            { return this.dateOfBirth;        }
	public void      setDateOfBirth(final LocalDate dateOfBirth) { this.dateOfBirth = dateOfBirth; }

	public Address   getAddress()                                { return this.address;            }
	public void      setAddress(final Address address)           { this.address = address;         }


	@Override public String toString() {
		
		return this.getName();
		
	}

}
