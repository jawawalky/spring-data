package demo.spring.data.jpa;

import static java.time.Month.DECEMBER;
import static java.time.Month.JANUARY;
import static java.time.Month.NOVEMBER;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.spring.data.jpa.address.Address;
import demo.spring.data.jpa.customer.Customer;
import demo.spring.data.jpa.customer.CustomerRepository;

/**
 * <h1>The Application Class</h1>
 * 
 * This is the starting point of your application.
 * 
 * @author Franz Tost
 */
@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
	// fields /////
	
	@Autowired private CustomerRepository customerRepository;
	
	
	// methods /////

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override public void run(final String... args) throws Exception {
		
		this.populateDatabase();
		
		
		// Find customer by name /////
		
		System.out.println("A) Finding Bob ...");
		
		// TODO
		//
		//  o Find 'Bob Builder' and print his data on the console.
		//
		
		
		// Find customers by city /////

		System.out.println("B) Finding customers in New York ...");
		
		// TODO
		//
		//  o Find all customers, who live in New York, and print
		//    them on the console.
		//
		
	}
	
	private void populateDatabase() {
		
		System.out.println("Populating database ...");
		
		this.customerRepository.save(
			new Customer(
				"Bob Builder",
				LocalDate.of(1980, NOVEMBER, 7),
				new Address("5th Avenue", "28438", "New York")
			)
		);
			
		this.customerRepository.save(
			new Customer(
				"Sue Smart",
				LocalDate.of(1989, JANUARY, 12),
				new Address("3 Oxford Street", "NW3 8QR", "London")
			)
		);
				
		this.customerRepository.save(
			new Customer(
				"Ann Artful",
				LocalDate.of(1999, DECEMBER, 8),
				new Address("Broadway", "28334", "New York")
			)
		);
				
	}
	
	private void print(final Customer customer) {
		
		System.out.printf(
			"[%d] %s (%s) lives in %s",
			customer.getId(),
			customer.getName(),
			customer.getDateOfBirth().format(DateTimeFormatter.ISO_LOCAL_DATE),
			customer.getAddress().toString()
		);
		System.out.println();
		
	}
	
}
