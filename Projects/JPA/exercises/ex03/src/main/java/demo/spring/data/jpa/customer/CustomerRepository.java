package demo.spring.data.jpa.customer;

import org.springframework.data.repository.CrudRepository;

/**
 * <h1>The Repository Class</h1>
 * 
 * Query methods, which match named queries.
 * 
 * @author Franz Tost
 *
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	// methods /////
	
	// TODO
	//
	//  o Define appropriate query methods for the two named queries that
	//    you defined in the 'Customer' class.
	//
	
}
